package com.myzee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

public class TestComparable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee e1 = new Employee(10, "nag");
		Employee e2 = new Employee(38, "pat");
		Employee e3 = new Employee(176, "bee");
		Employee e4 = new Employee(144, "fly");
		Employee e5 = new Employee(3, "xyz");
		
		TreeSet<Employee> t = new TreeSet<>();
		t.add(e1);
		t.add(e2);
		t.add(e3);
		t.add(e4);
		t.add(e5);
		
		System.out.println(t);
		
		Employee[] arr = new Employee[5];
		arr[0] = e1;
		arr[1] = e2;
		arr[2] = e3;
		arr[3] = e4;
		arr[4] = e5;
		
		Arrays.sort(arr);
		for(Employee e:arr) {
			System.out.println(e);
		}
		
		// =====using list=====
		System.out.println("Using List");
		List<Employee> l = new ArrayList<Employee>();
		l.add(e1);
		l.add(e2);
		l.add(e3);
		l.add(e4);
		l.add(e5);
		
		Collections.sort(l);	// Employee should be implemented with Comparable interface, else will get error
		l.forEach(e -> System.out.println(e));
	}

}
