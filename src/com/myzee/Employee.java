package com.myzee;

public class Employee implements Comparable<Employee>{
	int eid;
	String ename;
	public Employee(int eid, String ename) {
		super();
		this.eid = eid;
		this.ename = ename;
	}
	
	@Override
	public int compareTo(Employee o) {
		// TODO Auto-generated method stub
		int eid1 = this.eid;
		int eid2 = o.eid;
		
		if (eid1 < eid2) {
			return -1;
		} else if (eid1 > eid2) {
			return 1;
		} else {
			return 0;
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Employee [eid=" + eid + ", ename=" + ename + "]";
	}
	
	
	
}
